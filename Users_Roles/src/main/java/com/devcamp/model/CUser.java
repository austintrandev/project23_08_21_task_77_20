package com.devcamp.model;

import java.util.Date;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "users")
public class CUser {
	public CUser() {
		super();
	}

	public CUser(String username, String firstName, String lastName, String email, Date createDate, Date updateDate) {
		super();
		this.username = username;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.createDate = createDate;
		this.updateDate = updateDate;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@NotNull(message = "Chưa nhập username")
	@Column(name = "user_name", unique = true)
	private String username;

	@NotEmpty(message = "Chưa nhập firstname")
	@Column(name = "first_name")
	private String firstName;

	@NotEmpty(message = "Chưa nhập lastname")
	@Column(name = "last_name")
	private String lastName;

	@Column(name = "email")
	private String email;

	@ManyToOne
	@JsonIgnore
	private CRole roles;

	@NotEmpty(message = "Chưa nhập createDate")
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_date", nullable = true, updatable = false)
	@CreatedDate
	@JsonFormat(pattern = "dd-MM-yyyy")
	private Date createDate;

	@NotEmpty(message = "Chưa nhập updateDate")
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "update_date", nullable = true)
	@LastModifiedDate
	@JsonFormat(pattern = "dd-MM-yyyy")
	private Date updateDate;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public CRole getRoles() {
		return roles;
	}

	public void setRoles(CRole roles) {
		this.roles = roles;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
}
