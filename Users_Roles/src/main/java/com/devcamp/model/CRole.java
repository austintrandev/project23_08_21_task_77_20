package com.devcamp.model;

import java.util.Date;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name="roles")
public class CRole {
	public CRole() {
		super();
	}

	public CRole(String roleCode, String roleName, Date createDate, Date updateDate) {
		super();
		this.roleCode = roleCode;
		this.roleName = roleName;
		this.createDate = createDate;
		this.updateDate = updateDate;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@NotNull(message = "Chưa nhập roleCode")
	@Column(name="role_code")
	private String roleCode;
	
	@NotNull(message = "Chưa nhập roleName")
	@Column(name="role_name")
	private String roleName;
	
	@NotEmpty(message = "Chưa nhập createDate")
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_date", nullable = true, updatable = false)
	@CreatedDate
	@JsonFormat(pattern = "dd-MM-yyyy")
	private Date createDate;
	
	@NotEmpty(message = "Chưa nhập updateDate")
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "update_date", nullable = true)
	@LastModifiedDate
	@JsonFormat(pattern = "dd-MM-yyyy")
	private Date updateDate;
	
	@OneToMany(targetEntity = CUser.class, cascade = CascadeType.ALL)
	@JoinColumn(name = "roles_id")
	private List<CUser> users;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getRoleCode() {
		return roleCode;
	}

	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public List<CUser> getUsers() {
		return users;
	}

	public void setUsers(List<CUser> users) {
		this.users = users;
	}
}
