-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 23, 2021 at 06:51 AM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 7.3.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pizza365`
--

-- --------------------------------------------------------

--
-- Table structure for table `hibernate_sequence`
--

CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `hibernate_sequence`
--

INSERT INTO `hibernate_sequence` (`next_val`) VALUES
(1);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) NOT NULL,
  `create_date` datetime DEFAULT NULL,
  `role_code` varchar(255) NOT NULL,
  `role_name` varchar(255) NOT NULL,
  `update_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `create_date`, `role_code`, `role_name`, `update_date`) VALUES
(1, '2021-08-23 06:39:27', 'AD', 'Admin', '2021-08-23 06:39:27'),
(2, '2021-08-23 06:39:27', 'MAN', 'Manager', '2021-08-23 06:39:27'),
(3, '2021-08-23 06:39:27', 'STA', 'Staff', '2021-08-23 06:39:27'),
(4, '2021-08-23 06:39:27', 'ED', 'Editor', '2021-08-23 06:39:27'),
(5, '2021-08-23 06:39:27', 'GUE', 'Guest', '2021-08-23 06:39:27');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) NOT NULL,
  `create_date` datetime DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `user_name` varchar(255) NOT NULL,
  `roles_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `create_date`, `email`, `first_name`, `last_name`, `update_date`, `user_name`, `roles_id`) VALUES
(1, '2021-08-23 06:44:43', 'duc_nguyen@gmail.com', 'Nguyễn Trung', 'Đức', '2021-08-23 06:44:43', 'ducnt', 1),
(2, '2021-08-23 06:44:43', 'tin_pham@gmail.com', 'Phạm Trung', 'Tín', '2021-08-23 06:44:43', 'tinpt', 2),
(3, '2021-08-23 06:44:43', 'dinh_tran@gmail.com', 'Trần Lê', 'Đình', '2021-08-23 06:44:43', 'dinhtl', 3),
(4, '2021-08-23 06:44:43', 'anh_ngo@gmail.com', 'Ngô Thị Lan', 'Anh', '2021-08-23 06:44:43', 'anhntl', 4),
(5, '2021-08-23 06:44:43', 'phach_nguyen@gmail.com', 'Nguyễn Ngọc', 'Phách', '2021-08-23 06:44:43', 'phachnn', 5);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_k8d0f2n7n88w1a16yhua64onx` (`user_name`),
  ADD KEY `FKbgvg7xuekkcqmpvi3tgkxk85j` (`roles_id`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `FKbgvg7xuekkcqmpvi3tgkxk85j` FOREIGN KEY (`roles_id`) REFERENCES `roles` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
